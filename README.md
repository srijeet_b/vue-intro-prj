# case-changer

## To Try The App on the Go

> [Click here 🔗](https://dreamy-mayer-478ddd.netlify.app)

## To Run App in Local Environment
```
1. Open a terminal and run 
> git clone https://srijeet_b@bitbucket.org/srijeet_b/vue-intro-prj.git

2. cd vue-intro-prj

3. npm install && npm run serve

4. Open the endpoint shown in the terminal 
```
